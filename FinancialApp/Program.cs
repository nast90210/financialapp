﻿using System;
using System.IO;
using System.Net.Http;
using FinancialApp.Models;
using Microsoft.Extensions.Caching.Memory;

namespace FinancialApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var currencyConverter = new ExchangeRatesApiConverter(new HttpClient(), 
                new MemoryCache(new MemoryCacheOptions()), 
                "a5cf9da55cb835d0a633a7825b3aa8b5");

            // var USD_RUB = currencyConverter.ConvertCurrency(new CurrencyAmount("USD", 10),"RUB");
            // var EUR_USD = currencyConverter.ConvertCurrency(new CurrencyAmount("EUR", 10),"USD");
            
            var transactionRepository = new InFileTransactionRepository(Path.GetTempPath() + "InFileTransactions.txt");
            var transactionParser = new TransactionParser();

            var budgetApp = new BudgetApplication(transactionRepository, transactionParser, currencyConverter);

            budgetApp.AddTransaction("Трата -400 RUB Продукты Пятерочка");
            budgetApp.AddTransaction("Перевод 2000 RUB Маме HI");
            budgetApp.AddTransaction("Зачисление 5000 EUR Зарплата");
            
            budgetApp.OutputTransactions();
            
            budgetApp.OutputBalanceInCurrency("USD");

            

        }
    }
}
