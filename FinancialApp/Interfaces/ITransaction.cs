﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }
        ICurrencyAmount Amount { get; }
    }
}
