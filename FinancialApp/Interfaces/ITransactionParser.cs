﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
