﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction);
        ITransaction[] GetTransactions();
    }
}
