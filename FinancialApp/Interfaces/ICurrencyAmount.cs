﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get;  }
        decimal Amount { get; }
    }
}
