﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp
{
    public interface ICurrencyConverter
    {
        ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode);
    }
}
