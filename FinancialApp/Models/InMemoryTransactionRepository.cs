﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp.Models
{
    class InMemoryTransactionRepository : ITransactionRepository
    {
        private List<ITransaction> _inMemoryTransactions;

        public InMemoryTransactionRepository()
        {
            _inMemoryTransactions = new List<ITransaction>();
        }

        public void AddTransaction(ITransaction transaction)
        {
            if (transaction != null)
            {
                _inMemoryTransactions.Add(transaction);
            }
        }
        public ITransaction[] GetTransactions()
        {
            return _inMemoryTransactions.ToArray();
        }
    }
}
