﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using FinancialApp;

namespace FinancialApp.Models
{
    class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;
        private readonly string _apiKey;
        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _apiKey = apiKey;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            var exchangeRates = _memoryCache.GetOrCreate("EUR", entry =>
            {
                // Не хотелось пересматривать вэбинар - вроде лектор говорил о тайм-ауте в 10 минут
                // Но могу ошибаться =) В ДЗ этого параметра не нашел
                entry.SlidingExpiration = TimeSpan.FromMinutes(10);
                return GetExchangeRatesAsync();
            }).Result;

            if (currencyCode == amount.CurrencyCode)
            {
                Console.WriteLine("Нельзя конвертировать в ту же самую валюту");
                // Нужно ли выбрасывать ошибку? Может достаточно Warning
                // throw new ArgumentException("Нельзя конвертировать в ту же самую валюту");
                return amount;
            }

            bool hasCurrencyCode = exchangeRates.Rates.TryGetValue(currencyCode, out var rate);
            // var rates = exchangeRates.Rates.Where(ex => ex.Key.Contains(currencyCode))
            //     .Select(ex => ex.Value);
            
            // Кроскурсовый обмен - ЭТО ОЧЕНЬ НЕ ХОРОШО МЯГКО ГОВОРЯ. 
            // Наверное логично было бы использовать
            // http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}.base={amount.CurrencyCode}
            // и сохранять в MemoryCache для каждой из валют со своим Key
            if (amount.CurrencyCode != "EUR")
            {
                var hasTransCurrencyCode = exchangeRates.Rates.TryGetValue(amount.CurrencyCode, out var transRate);
                rate = hasTransCurrencyCode ? rate / transRate 
                    : throw new Exception($"Неизвестная валюта: {amount.CurrencyCode}");
            }

            return hasCurrencyCode ? new CurrencyAmount(currencyCode, amount.Amount * Convert.ToDecimal(rate)) 
                : throw new Exception($"Неизвестная валюта: {currencyCode}");
        }
        
        private async Task<ExchangeRatesApiResponse> GetExchangeRatesAsync()
        {
            Console.WriteLine("Request to exchangeratesapi is sending");
            var response = await _httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}");
            Console.WriteLine("Response from exchangeratesapi is received");
            response = response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(json);
        }
    }
}
