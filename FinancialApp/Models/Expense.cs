﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp.Models
{
    class Expense : ITransaction
    {
        public string Category { get; }
        public string Destination { get; }
        public DateTimeOffset Date { get; }
        public ICurrencyAmount Amount { get; }

        public Expense(ICurrencyAmount amount, DateTimeOffset date, string category, string destination)
        {
            // TODO: null проверка
            Category = category;
            Destination = destination;
            Date = date;
            Amount = amount;
        }
        
        // Либо добавить метод Save()
        // public override string ToString() => $"Трата {Amount} в {Destination} по категории {Category}";
        public override string ToString() => $"Трата {Amount} {Destination} {Category}";
    }
}
