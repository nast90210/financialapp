using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FinancialApp.Models
{
    public class InFileTransactionRepository : ITransactionRepository
    {
        private readonly string _transactionFile;
        
        public InFileTransactionRepository(string transactionFile)
        {
            _transactionFile = transactionFile;
            
            if(File.Exists(_transactionFile))
                File.Delete(_transactionFile);
        }
        public void AddTransaction(ITransaction transaction)
        {
            File.AppendAllText(_transactionFile,transaction.ToString() + Environment.NewLine);
        }

        public ITransaction[] GetTransactions()
        {
            List<ITransaction> transactionCollection = new List<ITransaction>();

            var transactionParser = new TransactionParser();
            
            foreach (var transaction in File.ReadAllLines(_transactionFile))
            {
                transactionCollection.Add(transactionParser.Parse(transaction));
            }
            
            return transactionCollection.ToArray();
        }
    }
}