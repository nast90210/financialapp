﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialApp.Models
{
    class BudgetApplication : IBudgetApplication
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionParser _transactionParser;
        private readonly ICurrencyConverter _currencyConverter;

        public BudgetApplication(ITransactionRepository transactionRepository, ITransactionParser transactionParser, ICurrencyConverter currencyConverter)
        {
            if (transactionRepository == null || transactionParser == null || currencyConverter == null)
            {
                throw new ArgumentNullException();
            }
            _transactionRepository = transactionRepository;
            _transactionParser = transactionParser;
            _currencyConverter = currencyConverter;
        }

        public void AddTransaction(string input)
        {
            var transaction = _transactionParser.Parse(input);
            _transactionRepository.AddTransaction(transaction);
        }

        public void OutputTransactions()
        {
            var allTransactions = _transactionRepository.GetTransactions();
            foreach (var transaction in allTransactions)
            {
                Console.WriteLine(transaction);
            }
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            if (String.IsNullOrEmpty(currencyCode))
            {
                throw new ArgumentNullException();
            }

            ITransaction[] allTransactions = _transactionRepository.GetTransactions();
            decimal sum = 0;

            foreach (var transaction in allTransactions)
            {
                ICurrencyAmount amountInCurrency = _currencyConverter.ConvertCurrency(transaction.Amount, currencyCode);
                sum += amountInCurrency.Amount;
            }

            Console.WriteLine($"Общий баланс в {currencyCode}: " + sum);
        }
    }
}
