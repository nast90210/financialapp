using System;

namespace FinancialApp.Models
{
    public class Income : ITransaction
    {
        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Source { get; }

        public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
        {
            Amount = amount;
            Date = date;
            Source = source;
        }

        // Либо добавить метод Save()
        // public override string ToString() => $"Зачисление {Amount} от {Source}";
        public override string ToString() => $"Зачисление {Amount} {Source}";
    }
}